package org.zpcat.ble;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.impl.conn.tsccm.WaitingThread;
import org.zpcat.ble.utils.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

/**
 * Created by Denis on 02.07.14.
 */
public class Serialport extends Activity {
    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothServerSocket server;
    private TextView mConnectionState;
    private TextView mDataField;
    private TextView mRssiField;
    private String mDeviceName;
    private String mDeviceAddress;
    private BluetoothSocket btSocket = null;
    private ExpandableListView mGattServicesList;
    private BluetoothLeService mBluetoothLeService;
    private boolean mConnected = false;
    private OutputStream outStream = null;
    private InputStream inStream = null;
    private static final UUID MY_UUID_SECURE = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";
    Button btnClient, btnServer,btnClientRead,btnClientWrite;
    public void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.serialport);

        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);

        mDataField = (TextView)findViewById(R.id.text);
        btnClient = (Button) findViewById(R.id.btnClient);
        btnServer = (Button) findViewById(R.id.btnServer);
        btnClientRead = (Button) findViewById(R.id.btnClientRead);
        btnClientWrite = (Button) findViewById(R.id.btnClientWrite);
            btnClient.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    Client();
                }
            });

        btnClientRead.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Read();
            }
        });
        btnClientWrite.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Send();
            }
        });

        btnServer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

              Server();
            }
        });




}
    private void Send() {
        byte[] msgBuffer = "Hello".getBytes();


        try {
            outStream.write(msgBuffer);
        } catch (IOException e) {

            Toast.makeText(getBaseContext(), "Ошибка", Toast.LENGTH_SHORT).show();
        }
    }
    private void Read() {
        try {

            byte[] buffer = new byte[256];  // buffer store for the stream
            int bytes; // bytes returned from read()

                    // Read from the InputStream
                    bytes = inStream.read(buffer);
                    String strIncom = new String(buffer, 0, bytes);
                    mDataField.setText(mDataField.getText()+strIncom);


        } catch (IOException e) {

            Toast.makeText(getBaseContext(), "Ошибка", Toast.LENGTH_SHORT).show();
        }



    }
    public void Client()
    {
        mBluetoothAdapter =   BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mDeviceAddress);
        ConnectThread nmConnectedThread = new ConnectThread(device);
        nmConnectedThread.start();
        Toast.makeText(getBaseContext(), "первая попытка", Toast.LENGTH_SHORT).show();


    }
    public void Client2
            (){
        mBluetoothAdapter =   BluetoothAdapter.getDefaultAdapter();

        // Automatically connects to the device upon successful start-up initialization.
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mDeviceAddress);
        try {
            btSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
        } catch (IOException e) {

        }

        // Discovery is resource intensive.  Make sure it isn't going on
        // when you attempt to connect and pass your message.
        mBluetoothAdapter.cancelDiscovery();

        // Establish the connection.  This will block until it connects.
        try {

            btSocket.connect();
            Log.d("...Соединение установлено и готово к передачи данных...");
        } catch (IOException e) {
            try {
                btSocket.close();
            } catch (IOException e2) {
                Integer k = 6;
            }
        }

        // Create a data stream so we can talk to server.


        try {
            outStream = btSocket.getOutputStream();
            inStream = btSocket.getInputStream();
        } catch (IOException e) {
        }
        Toast.makeText(getBaseContext(), "Подключились", Toast.LENGTH_SHORT).show();
    }
    public void Server(){
        mBluetoothAdapter =   BluetoothAdapter.getDefaultAdapter();

        try {
            server = mBluetoothAdapter.listenUsingRfcommWithServiceRecord("DEnis", MY_UUID);
        } catch (IOException e) {

        }

        // Discovery is resource intensive.  Make sure it isn't going on
        // when you attempt to connect and pass your message.

        // Establish the connection.  This will block until it connects.
        try {
            btSocket = server.accept();
            Log.d("...Соединение установлено и готово к передачи данных...");
        } catch (IOException e) {
            try {
                btSocket.close();
            } catch (IOException e2) {

            }
        }

        // Create a data stream so we can talk to server.


        try {
            outStream = btSocket.getOutputStream();
            inStream = btSocket.getInputStream();
        } catch (IOException e) {
        }
        Toast.makeText(getBaseContext(), "Подключились", Toast.LENGTH_SHORT).show();
    }

    public  class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device) {
            // Use a temporary object that is later assigned to mmSocket,
            // because mmSocket is final
            BluetoothSocket tmp = null;
            mmDevice = device;

            // Get a BluetoothSocket to connect with the given BluetoothDevice
            try {
                // MY_UUID is the app's UUID string, also used by the server code
                tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) { }
            mmSocket = tmp;
        }

        public void run() {
            // Cancel discovery because it will slow down the connection

            try {
                // Connect the device through the socket. This will block
                // until it succeeds or throws an exception
                mBluetoothAdapter.cancelDiscovery();
                mmSocket.connect();
                Toast.makeText(getBaseContext(), "Подключились", Toast.LENGTH_SHORT).show();
            } catch (Exception connectException) {
                // Unable to connect; close the socket and get out
                try {
                    mmSocket.close();
                } catch (IOException closeException) { }
                return;
            }


            // Do work to manage the connection (in a separate thread)
        }

        /** Will cancel an in-progress connection, and close the socket */
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) { }
        }
    }
}