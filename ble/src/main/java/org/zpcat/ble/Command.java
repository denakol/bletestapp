package org.zpcat.ble;

/**
 * Created by Denis on 08.08.2014
 */
public  class Command {
    public static byte[] GetComand(TypeCommand type)
    {
        byte[] command = new byte[18];
        command[0]= (byte)0xCC;
        command[1]= 0x0C;
        command[2]=0;
        command[3]=18;
        switch (type)
        {
            case Enable:
            {
                command[4]=0x01;
                command[5]=0x20;
                break;
            }
            case Disable:
            {
                command[4]=0x01;
                command[5]=0x10;
                break;
            }
            case Run:
            {
                command[4]=0x02;
                command[5]=0x20;
                break;
            }
            case Stop:
            {
                command[4]=0x02;
                command[5]=0x10;
                break;
            }
            case TimeSync:
            {
                command[4]=0x05;
                break;
            }
            case ReadStandart:
            {
                command[4]=(byte)0x83;
                break;
            }
            case ReadExtend:
            {
                command[4]=(byte)0x84;
                break;
            }
        }
        char control=0;
        for(int i=0; i<15;i++)
        {
            control+=  (command[i] &0x00ff);
        }

        command[15] = (byte)((control >> 8) & 0xff);
        command[16] = (byte)(control & 0xff);

        command[17] = 0x0A;
        return command;
    }
}
enum TypeCommand {
     Enable,
     Disable,
     Run,
     Stop,
     TimeSync,
     ReadStandart,
     ReadExtend

}