/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.zpcat.ble;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.PendingIntent;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
//import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.zpcat.ble.utils.Log;


/**
 * For a given BLE device, this Activity provides the user interface to connect, display data,
 * and display GATT services and characteristics supported by the device.  The Activity
 * communicates with {@code BluetoothLeService}, which in turn interacts with the
 * Bluetooth LE API.
 */
public class DeviceControlActivity extends Activity {

    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";

    private TextView mConnectionState;
    private TextView mDataField;
    private TextView mRssiField;
    private String mDeviceName;
    private String mDeviceAddress;
    private ExpandableListView mGattServicesList;
    private BluetoothLeService mBluetoothLeService;
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    private boolean mConnected = false;
    private BluetoothGattCharacteristic mNotifyCharacteristic;
    private BluetoothGattCharacteristic mWriteCharacteristic;
    private BluetoothGattCharacteristic mCharacteristic;
    private boolean ExtendDate;
    byte[] array = new byte[0];
    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";
    Button btnEnable, btnDisable,btnRun,btnStop,btnSync, btnReadExtend;
    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e("Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBluetoothLeService.connect(mDeviceAddress);
            mBluetoothLeService.setBLEServiceCb(mDCServiceCb);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gatt_services_characteristics);

        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);

        // Sets up UI references.
        ((TextView) findViewById(R.id.device_address)).setText(mDeviceAddress);
        mGattServicesList = (ExpandableListView) findViewById(R.id.gatt_services_list);
        mGattServicesList.setOnChildClickListener(servicesListClickListner);
        mConnectionState = (TextView) findViewById(R.id.connection_state);
        mDataField = (TextView) findViewById(R.id.data_value);
        mRssiField = (TextView) findViewById(R.id.signal_rssi);

        getActionBar().setTitle(mDeviceName);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);



        btnEnable = (Button) findViewById(R.id.btnEnable);
        btnDisable = (Button) findViewById(R.id.btnDisable);
        btnRun = (Button) findViewById(R.id.btnRun);
        btnStop = (Button) findViewById(R.id.btnStop);
        btnSync = (Button) findViewById(R.id.btnSync);
        btnReadExtend =(Button) findViewById(R.id.btnReadExtend);
        btnEnable.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                mCharacteristic.setValue(Command.GetComand(TypeCommand.Enable));
                mBluetoothLeService.writeCharacteristic(mCharacteristic);
            }
        });

        btnDisable.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mCharacteristic.setValue(Command.GetComand(TypeCommand.ReadStandart));
                mBluetoothLeService.writeCharacteristic(mCharacteristic);

            }
        });
        btnRun.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                mCharacteristic.setValue(Command.GetComand(TypeCommand.Run));
                mBluetoothLeService.writeCharacteristic(mCharacteristic);
            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                mCharacteristic.setValue(Command.GetComand(TypeCommand.Stop));
                mBluetoothLeService.writeCharacteristic(mCharacteristic);
            }
        });
        btnSync.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {

                mCharacteristic.setValue(Command.GetComand(TypeCommand.TimeSync));
                mBluetoothLeService.writeCharacteristic(mCharacteristic);
            }
        });
        btnReadExtend.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                mDataField.setText("");
                ExtendDate = true;
                mCharacteristic.setValue(Command.GetComand(TypeCommand.ReadExtend));
                mBluetoothLeService.writeCharacteristic(mCharacteristic);
            }
        });
    }







    // If a given GATT characteristic is selected, check for supported features.  This sample
    // demonstrates 'Read' and 'Notify' features.  See
    // http://d.android.com/reference/android/bluetooth/BluetoothGatt.html for the complete
    // list of supported characteristic features.
    private final ExpandableListView.OnChildClickListener servicesListClickListner =
            new ExpandableListView.OnChildClickListener() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                            int childPosition, long id) {
                    if (mGattCharacteristics != null) {
                        final BluetoothGattCharacteristic characteristic =
                                mGattCharacteristics.get(groupPosition).get(childPosition);
                        final int charaProp = characteristic.getProperties();
                        if ((charaProp & BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                            // If there is an active notification on a characteristic, clear
                            // it first so it doesn't update the data field on the user interface.
                            Log.d("BluetoothGattCharacteristic has PROPERTY_READ, so send read request");


                            mBluetoothLeService.readCharacteristic(characteristic);
                        }

                        if ((charaProp & BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                            Log.d("BluetoothGattCharacteristic has PROPERTY_NOTIFY, so send notify request");

                            mNotifyCharacteristic = characteristic;
                            mBluetoothLeService.setCharacteristicNotification(
                                    characteristic, true);
                        }

                        if (((charaProp & BluetoothGattCharacteristic.PROPERTY_WRITE) |
                                (charaProp & BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE)) > 0) {
                            Log.d("BluetoothGattCharacteristic has PROPERY_WRITE | PROPERTY_WRITE_NO_RESPONSE");

                            mWriteCharacteristic = characteristic;
                            // popup an dialog to write something.
                            byte[] strBytes = "hello".getBytes();
                            characteristic.setValue(strBytes);
                            mBluetoothLeService.writeCharacteristic(characteristic);
                        }

                        return true;
                    }
                    return false;
                }
    };



    private void writeCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothLeService != null) {
            mBluetoothLeService.writeCharacteristic(characteristic);
        }
    }

    private void clearUI() {
        mGattServicesList.setAdapter((SimpleExpandableListAdapter) null);
        mDataField.setText(R.string.no_data);
    }



    @Override
    protected void onResume() {
        super.onResume();
        //registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (mBluetoothLeService != null) {
            final boolean result = mBluetoothLeService.connect(mDeviceAddress);
            Log.d("Connect request result=" + result);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        //unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnection);
        mBluetoothLeService = null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gatt_services, menu);
        if (mConnected) {
            menu.findItem(R.id.menu_connect).setVisible(false);
            menu.findItem(R.id.menu_disconnect).setVisible(true);
        } else {
            menu.findItem(R.id.menu_connect).setVisible(true);
            menu.findItem(R.id.menu_disconnect).setVisible(false);
        }
        return true;
    }

    @TargetApi(Build.VERSION_CODES.ECLAIR)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_connect:
                mBluetoothLeService.connect(mDeviceAddress);
                return true;
            case R.id.menu_disconnect:
                mBluetoothLeService.disconnect();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateConnectionState(final int resourceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mConnectionState.setText(resourceId);
            }
        });
    }

    private void displayData(String data, byte[] dataByte) {
        if (data != null) {
            if(!ExtendDate)
            {
                mDataField.setText("");
                array = new byte[0];
            }
            mDataField.setText(mDataField.getText()+data);
            byte[] arrayTemp = new byte[array.length + dataByte.length];
            System.arraycopy(array,0,arrayTemp,0,array.length);
            System.arraycopy(dataByte,0,arrayTemp,array.length,dataByte.length);
            array= arrayTemp;
            if(dataByte[dataByte.length-1]==0x0A &&ExtendDate)
            {

                ExtendDate=false;
                int num = array.length;
              /*  GraphViewData[] datad = new GraphViewData[num];
                double v=0;
                for (int i=0; i<num; i++) {

                    datad[i] = new GraphViewData(i, array[i]);
                }
                GraphView graphView = new LineGraphView(
                        this
                        , "GraphViewDemo"
                );
// add data
                graphView.addSeries(new GraphViewSeries(datad));
// set view port, start=2, size=40
                graphView.setViewPort(2, 40);
                graphView.setScrollable(true);
// optional - activate scaling / zooming
                graphView.setScalable(true);
                graphView.getGraphViewStyle().setTextSize(14);
                graphView.getGraphViewStyle().setVerticalLabelsWidth(30);
                graphView.getGraphViewStyle().setNumVerticalLabels(10);
                graphView.setManualYAxisBounds(9,0);
                LinearLayout layout = (LinearLayout) findViewById(R.id.layout);
                layout.addView(graphView);*/
            }
        }
    }

    private void displayRssi(String rssi) {
        if (rssi != null) {
            //Log.d("-- dispaly Rssi: " + rssi);
            mRssiField.setText(rssi);
        }
    }
    // Demonstrates how to iterate through the supported GATT Services/Characteristics.
    // In this sample, we populate the data structure that is bound to the ExpandableListView
    // on the UI.
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private void displayGattServices(List<BluetoothGattService> gattServices) {
        Log.d("displayGATTServices");

        if (gattServices == null) return;
        String uuid = null;
        String unknownServiceString = getResources().getString(R.string.unknown_service);
        String unknownCharaString = getResources().getString(R.string.unknown_characteristic);
        ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<HashMap<String, String>>();
        ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData
                = new ArrayList<ArrayList<HashMap<String, String>>>();
        mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
            HashMap<String, String> currentServiceData = new HashMap<String, String>();
            uuid = gattService.getUuid().toString();
            String uid = gattService.getUuid().toString();
            currentServiceData.put(
                    LIST_NAME, SampleGattAttributes.lookup(uuid, uuid));
            currentServiceData.put(LIST_UUID, uuid);
            gattServiceData.add(currentServiceData);

            ArrayList<HashMap<String, String>> gattCharacteristicGroupData =
                    new ArrayList<HashMap<String, String>>();
            List<BluetoothGattCharacteristic> gattCharacteristics =
                    gattService.getCharacteristics();
            ArrayList<BluetoothGattCharacteristic> charas =
                    new ArrayList<BluetoothGattCharacteristic>();

            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                charas.add(gattCharacteristic);
                HashMap<String, String> currentCharaData = new HashMap<String, String>();
                int uuidd = gattCharacteristic.getUuid().hashCode();
                currentCharaData.put(
                        LIST_NAME, SampleGattAttributes.lookup(uuid, uuid));
                currentCharaData.put(LIST_UUID, uuid);
                gattCharacteristicGroupData.add(currentCharaData);

                if (uuidd== 1813384059) {
                final int charaProp = gattCharacteristic.getProperties();

                    if ((charaProp & BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                        Log.d("BluetoothGattCharacteristic has PROPERTY_NOTIFY, so send notify request");

                        mNotifyCharacteristic = gattCharacteristic;
                        mBluetoothLeService.setCharacteristicNotification(
                                gattCharacteristic, true);
                    }
                    mCharacteristic = gattCharacteristic;

            }
            }
            mGattCharacteristics.add(charas);
            gattCharacteristicData.add(gattCharacteristicGroupData);
        }

        SimpleExpandableListAdapter gattServiceAdapter = new SimpleExpandableListAdapter(
                this,
                gattServiceData,
                android.R.layout.simple_expandable_list_item_2,
                new String[] {LIST_NAME, LIST_UUID},
                new int[] { android.R.id.text1, android.R.id.text2 },
                gattCharacteristicData,
                android.R.layout.simple_expandable_list_item_2,
                new String[] {LIST_NAME, LIST_UUID},
                new int[] { android.R.id.text1, android.R.id.text2 }
        );
        mGattServicesList.setAdapter(gattServiceAdapter);
    }

    private DCServiceCb mDCServiceCb = new DCServiceCb();

    public class DCServiceCb implements BluetoothLeService.BLEServiceCallback {

        @Override
        public void displayRssi(final int rssi) {
            runOnUiThread(new Runnable() {
                              @Override
                              public void run() {
                                  DeviceControlActivity.this.displayRssi(String.valueOf(rssi));
                              }
                          }
            );

        }

        @Override
        public void displayData(final String data, final byte[] dataByte) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DeviceControlActivity.this.displayData(data,dataByte);
                }
            });

        }

        @Override
        public void notifyConnectedGATT() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mConnected = true;
                    updateConnectionState(R.string.connected);
                    invalidateOptionsMenu();
                }
            });

        }

        @Override
        public void notifyDisconnectedGATT() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mConnected = false;
                    updateConnectionState(R.string.disconnected);
                    invalidateOptionsMenu();
                    clearUI();
                }
            });
        }

        @Override
        public void displayGATTServices() {
            Log.d("displayGATTServices.");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mBluetoothLeService != null) {
                        DeviceControlActivity.this.displayGattServices(
                                mBluetoothLeService.getSupportedGattServices());
                    }
                }
            });
        }
    }


}
